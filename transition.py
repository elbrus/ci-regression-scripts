#! /usr/bin/python3

import yaml
import collections

FILE = "/home/paul/tmp/packages.yaml"
with open(FILE, 'r', encoding='utf-8') as stream:
    packages = yaml.safe_load(stream)

#print(packages)

transitions = collections.defaultdict(dict)
pkgs = collections.defaultdict(dict)
pkgsets = collections.defaultdict(dict)

acked = set()
a = acked
a.add('prompt-toolkit_2.x')
a.add('auto-exiv2')

requested = set()
r = requested

skip_trans = set()
s = skip_trans
s.add('haskell')
s.add('ocaml')
s.add('rust')
s.add('python2-rm')        # not a real transition
# auto-exiv2               # all ftbfs bugs filed removal on 27-02 and 13-03
s.add('auto-libgclib')     # ??
s.add('auto-libgit2')      # debcargo
# boost-python                   pythonmagick (23-02)
s.add('enchant-2')         # not a real transition
s.add('libncurses6')       # nvidia
s.add('libqt5quick5-gles') # not a real transition: gst-plugins-good1.0, qt-gstreamer, ktp-call-ui (26-02), gammaray (24-02), pyside2 (13-03), plasma-desktop
s.add('python3.8')         # all reds are filed; macs (OOS: 2019-12-28), python-cutadapt

# auto-ndpi                      FTBFS arm64 i386 ppc64el s390x (30-12)
# auto-python-socksipychain      failing autopkgtest 13-02
#
#
# llvm-defaults-9                not started
# prompt-toolkit_2.x             mycli (aging 12-02)
# python2-rm                     none

def getColor(tkey, transitions, requested):
    status = transitions[tkey]['status']
    if status == 'planned':
        if tkey in acked:
            tcolor = 'green'
        elif tkey in requested:
            tcolor = 'yellow'
        else:
            tcolor = 'lightblue'
    elif status == 'ongoing':
        tcolor = 'red'
    elif status == 'finished':
        tcolor = 'gray'
    elif status == 'permanent':
        raise()
    else:
        raise()
    return tcolor

for package in packages:
    name = package['name']
    for trans in package['list']:
        if trans[0] not in skip_trans:
            transitions.setdefault(trans[0], {}).setdefault('packages', set()).add(name)
            transitions.setdefault(trans[0], {}).setdefault('status', trans[1])
            pkgs.setdefault(name, set()).add(trans[0])

for [pkg, trans] in pkgs.items():
    if len(trans) > 1:
        pkgsets.setdefault(str(sorted(trans)), set()).add(pkg)

print('digraph x {')
print('    rankdir=LR;')
print('    node [style=filled,color=lightgray];')
for [tkey, trans] in transitions.items():
    tcolor = getColor(tkey, transitions, requested)
    print('    "' + tkey + '" [color=' + tcolor + "];")

for [transcom, pkgs] in pkgsets.items():
    bPrint = True
    transstr = transcom.strip('[]').replace(', ', '\n').replace("'",'')
    for tr in transcom.strip('[]').split(', '):
        trans = tr.strip("'")
        print('    "' + trans + '" -> "' + transstr + '";')
        tcolor = getColor(trans, transitions, requested)
        if tcolor in ['green', 'red', 'blue']:
            print('    "' + transstr + '" [color=' + tcolor + '];')

        if bPrint:
            for pkg in pkgs:
                print('    "' + transstr + '" -> "' + pkg + '";')
            bPrint = False

print('}')
