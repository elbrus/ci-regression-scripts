#! /usr/bin/python3

from bs4 import BeautifulSoup
import copy
import datetime
from email.message import EmailMessage
import json
import optparse
import platform
import time
import subprocess
import yaml

if platform.node() == 'respighi':
    RESPIGHI = True
else:
    RESPIGHI = False

RES_EX_FILE = "/home/release/britney/var/data-b2/output/excuses.yaml"
if RESPIGHI:
    EX_FILE = RES_EX_FILE
    RES_FILE = "/home/elbrus/oos.log"
else:
    EX_FILE = "/home/paul/tmp/excuses.yaml"
    RES_FILE = "/home/paul/tmp/oos.log"

AGE_LIMIT = 30
NMU_ARCH_ALL = '''dgit clone %(pkg)s
dch --nmu "source only upload to enable migration (Closes: #%(bug)s)"
dch -r ""
git commit . -m"Prepare d/changelog for upload"
dgit --quilt=linear -wgf --delayed=5 push-source
'''
NMU_STRAIGHT = '''dgit clone %(pkg)s
dch --nmu "source only upload to enable migration"
dch -r ""
git commit . -m"Prepare d/changelog for upload"
dgit --quilt=linear -wgf push-source
'''
EMAIL_FROM = 'Paul Gevers <elbrus@debian.org>'
EMAIL_TO = '%(trigger)s@packages.debian.org, %(broken)s@packages.debian.org'
EMAIL_CC = EMAIL_FROM
EMAIL_SINGLE_SUBJECT = "src:%(pkg)s: fails to migrate to testing for too long"

EMAIL_NMU = '''Source: %(pkg)s
Version: %(old)s
Severity: serious
Control: close -1 %(new)s
Tags: sid trixie pending
User: release.debian.org@packages.debian.org
Usertags: out-of-sync

Dear maintainer(s),

The Release Team considers packages that are out-of-sync between testing and unstable for more than 30 days as having a Release Critical bug in testing [1]. Your package src:%(pkg)s has been trying to migrate for %(age)d days [2]. Hence, I am filing this bug.

If a package is out of sync between unstable and testing for a longer period, this usually means that bugs in the package in testing cannot be fixed via unstable. Additionally, blocked packages can have impact on other packages, which makes preparing for the release more difficult. Finally, it often exposes issues with the package and/or
its (reverse-)dependencies. We expect maintainers to fix issues that hamper the migration of their package in a timely manner.

This bug will trigger auto-removal when appropriate. As with all new bugs, there will be at least 30 days before the package is auto-removed.

I have immediately closed this bug with the version in unstable, so if that version or a later version migrates, this bug will no longer affect testing. I have also tagged this bug to only affect sid and trixie, so it doesn't affect (old-)stable.

Your package is only blocked because the arch:all binary package(s) aren't built on a buildd. Unfortunately the Debian infrastructure doesn't allow arch:all packages to be properly binNMU'ed. Hence, I will shortly do a no-changes source-only upload to DELAYED/5, closing this bug. Please let me know if I should delay or cancel that upload.

Paul

[1] https://lists.debian.org/debian-devel-announce/2023/06/msg00001.html
[2] https://qa.debian.org/excuses.php?package=%(pkg)s
'''

EMAIL_SINGLE = '''Source: %(pkg)s
Version: %(old)s
Severity: serious
Control: close -1 %(new)s
Tags: sid trixie
User: release.debian.org@packages.debian.org
Usertags: out-of-sync

Dear maintainer(s),

The Release Team considers packages that are out-of-sync between testing and unstable for more than 30 days as having a Release Critical bug in testing [1]. Your package src:%(pkg)s has been trying to migrate for %(age)d days [2], hence this bug report. The current output of the migration software for this package is copied to the bottom of this report and should list the reason why the package is blocked.

If a package is out of sync between unstable and testing for a longer period, this usually means that bugs in the package in testing cannot be fixed via unstable. Additionally, blocked packages can have impact on other packages, which makes preparing for the release more difficult. Finally, it often exposes issues with the package and/or its (reverse-)dependencies. We expect maintainers to fix issues that hamper the migration of their package in a timely manner.

This bug will trigger auto-removal when appropriate. As with all new bugs, there will be at least 30 days before the package is auto-removed.

This bug submission immediately closes the bug with the version in unstable, so if that version or a later version migrates, this bug will no longer affect testing. This bug is also tagged to only affect sid and trixie, so it doesn't affect (old-)stable.

If you believe your package is unable to migrate to testing due to issues beyond your control, don't hesitate to contact the Release Team.

This bug report has been automatically generated and has only been sent manually. If you have any comments with regards to the content or the process, please reach out to me.

Paul

[1] https://lists.debian.org/debian-devel-announce/2023/06/msg00001.html
[2] https://qa.debian.org/excuses.php?package=%(pkg)s

Current text from [2]:
%(excuse)s
'''

ACT = 0
OLD = 1
NEW = 2
BUG = 3
AGE = 4
EXC = 5
DATE = 6
COM = 7


def update():
    try:
        new_list = _load_info()
    except FileNotFoundError:
        new_list = {}

    for pkg, data in new_list.items():
        data[ACT] = False

    with open(EX_FILE, 'r', encoding='utf-8') as stream:
        ex = yaml.safe_load(stream)
    res = subprocess.run([
        'psql',
        "postgresql://udd-mirror:udd-mirror@udd-mirror.debian.net/udd",
        "-c",
        ("select source, testing_version, " +
         "unstable_version, sync from migrations where in_testing = " +
         "date(now()) and date(sync) < date(now()) - " +
         str(AGE_LIMIT) +
         " ;"
         )],
      capture_output=True, text=True, check=True)
    packages = res.stdout.splitlines()
    packages = packages[2:-2]
    stamp = time.strftime('%Y-%m-%d')
    now = datetime.datetime.now()
    for package in packages:
        info = package.split('|')
        pkg = info[0].strip()
        testing = info[1].strip()
        sid = info[2].strip()
        age = now - datetime.datetime.fromisoformat(info[3].strip())
        exc = None
        for excuse in ex['sources']:
            if excuse['item-name'] == pkg:
                html = ''
                for line in excuse['excuses']:
                    html += '\n' + line
                parsed_html = BeautifulSoup(html, features="lxml")
                exc = parsed_html.text
                break

        if pkg not in new_list:
            new_list[pkg] = [True, testing, sid, 0, age.days, exc, stamp, '']
        else:
            new_list[pkg][ACT] = True
            new_list[pkg][OLD] = testing
            new_list[pkg][NEW] = sid
            new_list[pkg][AGE] = age.days
            new_list[pkg][EXC] = exc

    _save_info(new_list)


def auto_detect():
    info = _load_info()
    for pkg in sorted(info, key=lambda x: info[x][AGE], reverse=True):
        if _should_show(info, pkg):
            print(pkg)
            break

    with open(EX_FILE, 'r', encoding='utf-8') as stream:
        ex = yaml.safe_load(stream)

    for excuse in ex['sources']:
        try:
            excuse['item-name']
        except KeyError:
            continue
        if excuse['item-name'] == pkg:
            break
    builtByJelmer = False
    noOtherIssue = True
    for pol, pol_info in excuse['policy_info'].items():
        if pol == 'builtonbuildd' and 'jelmer' in pol_info['signed-by'].values():
            builtByJelmer = True
        elif pol_info['verdict'] != 'PASS':
            noOtherIssue = False

    if builtByJelmer and noOtherIssue:
        nmu_arch_all(pkg, requires_bug_number=False)
        info[pkg][COM] = "CHECK: auto processed, should migrate in 5 days"
        _save_info(info)
    else:
        print(pkg + ' was *not* processed by auto_detect')


def _should_show(info, pkg):
    return info[pkg][ACT] and info[pkg][BUG] == 0 and \
        info[pkg][COM] == '' and not pkg.startswith('haskell-')


def show_unannotated(todo=False, quiet=False, check=False):
    if quiet:
        return
    info = _load_info()
    for pkg in sorted(info, key=lambda x: info[x][AGE]):
        if _should_show(info, pkg):
            print(pkg)
            print('    https://tracker.debian.org/pkg/' + pkg)
            print('    ' + info[pkg][OLD])
            print('    ' + info[pkg][NEW])
            print('    ' + str(info[pkg][AGE]))
            print('    ' + info[pkg][DATE])


def clean():
    info = _load_info()
    new_info = copy.deepcopy(info)  # copy because changing the content
    for pkg in info:
        if not info[pkg][ACT]:
            new_info.pop(pkg)

    _save_info(new_info)


def srchash(src):
    '''archive hash prefix for source package'''

    if src.startswith('lib'):
        return src[:4]
    else:
        return src[0]


def reportbug(pkg, nmu):
    info = _load_info()

    if info[pkg][BUG] != 0:
        print(pkg + ' already has a bug number; skipping')
        return
    old = info[pkg][OLD]
    new = info[pkg][NEW]
    age = info[pkg][AGE]
    exc = info[pkg][EXC]
    msg = EmailMessage()
    msg['From'] = EMAIL_FROM
    msg['To'] = 'submit@bugs.debian.org'
    msg['Subject'] = EMAIL_SINGLE_SUBJECT % {
        'pkg': pkg}
    if nmu:
        msg.set_content(EMAIL_NMU % {
            'pkg': pkg, 'old': old, 'new': new, 'age': age})
    else:
        msg.set_content(EMAIL_SINGLE % {
            'pkg': pkg, 'old': old, 'new': new, 'age': age, 'excuse': exc})
    with open('/tmp/message.eml', 'w') as f:
        print(msg, file=f)
    subprocess.run(['thunderbird', '/tmp/message.eml'])
    if nmu:
        time.sleep(120)


def nmu_arch_all(pkg, requires_bug_number=True):
    if requires_bug_number:
        update_bug_number
        info = _load_info()
        assert info[pkg][BUG] > 0, "Expected a bug number higher than 0"
        bug_str = str(info[pkg][BUG])
        print('nmu-arch-all ' + pkg + ' ' + bug_str)
        nmu_arch_all = NMU_ARCH_ALL % {'pkg': pkg, 'bug': bug_str}
    else:
        nmu_arch_all = NMU_STRAIGHT % {'pkg': pkg}
    tmp_pwd = '/tmp/'
    dgit_created_dir = False
    for line in nmu_arch_all.splitlines():
        print(line)
        res = subprocess.run(line, text=True, capture_output=True, \
                             cwd=tmp_pwd, check=True, shell=True)
        if not dgit_created_dir:
            tmp_pwd += pkg
            dgit_created_dir = True


def update_bug_number():
    info = _load_info()
    for pkg in info.keys():
        if _should_show(info, pkg):
            res = subprocess.run(['bts', 'select', 'usertag:out-of-sync', 'user:release.debian.org@packages.debian.org', 'source:' + pkg], text=True, capture_output=True)
            if res.returncode != 0:
                print("Couldn't run bts select")
                exit(1)
            if len(res.stdout) != 0:
                print('Updating bug number for package ' + pkg + ': ' + res.stdout)
                info[pkg][BUG] = int(res.stdout)
                _save_info(info)
            else:
                print('No results from "bts select ..." for %s' % pkg)


def _load_info():
    with open(RES_FILE) as f:
        regr_info = json.load(f)
    return regr_info


def _save_info(new_list):
    # new_list.sort(key=itemgetter(4))
    with open(RES_FILE, 'w') as f:
        json.dump(new_list, f, indent=4)


if __name__ == '__main__':
    parser = optparse.OptionParser(version="%prog")
    parser.add_option("-a", "--age", action="store", dest="age", type="int", default=AGE_LIMIT,
                          help="show items that were autotriggered in the list of results")
    parser.add_option(      "--auto", action="store_true", dest="auto", default=False,
                          help="try to autodetect what needs to be done with the oldest issue")
    parser.add_option("-b", "--bug", action="store_true", dest="bug", default=False,
                          help="report a bug against package(s)")
    parser.add_option(      "--check", action="store_true", dest="check", default=False,
                          help="show items that were annotated with CHECK")
    parser.add_option(      "--clean", action="store_true", dest="clean", default=False,
                          help="remove items that aren't active anymore")
    parser.add_option("-d", "--debug", action="store_true", dest="debug", default=False,
                          help="show debugging information")
    parser.add_option("-n", "--nmu-package", action="store_true", dest="nmu", default=False,
                          help="no-changes source-only NMU; implies --update-bug-number")
    parser.add_option("-q", "--quiet", action="store_true", dest="quiet", default=False,
                          help="don't show the output")
    parser.add_option("-t", "--todo", action="store_true", dest="todo", default=False,
                          help="show items that were annotated with TODO")
    parser.add_option("-u", "--update", action="store_true", dest="update", default=False,
                          help="update the regression log")
    parser.add_option(      "--update-bug-number", action="store_true", dest="bug_number", default=False,
                          help="query the bts and update bug number")
    (options, args) = parser.parse_args()
    if options.debug:
        print(options)
    if options.debug:
        print(args)
    if options.age != AGE_LIMIT:
        AGE_LIMIT = options.age
    if options.auto:
        if options.debug:
            print('Running auto()')
        auto_detect()
        exit()
    if options.update:
        if options.debug:
            print('Running update()')
        update()
    if options.clean:
        if options.debug:
            print('Running clean()')
        clean()
    for pkg in args:
        if options.bug:
            options.quiet = True
            if options.debug:
                print('Running reportbug(' + pkg + ', ' + str(options.nmu) + ')')
            reportbug(pkg, options.nmu)
        if options.nmu:
            options.quiet = True
            if options.debug:
                print('Running nmu_arch_all(' + pkg + ')')
            nmu_arch_all(pkg)
    if options.bug_number:
        if options.debug:
            print('Running update_bug_number()')
        update_bug_number()
    show_unannotated(todo=options.todo,
                     quiet=options.quiet,
                     check=options.check)
