#! /usr/bin/python3

import calendar
import collections
import copy
from email.message import EmailMessage
import json
import optparse
import platform
#from smtplib import SMTP
from string import Template
import time
import subprocess
import yaml
import re

shell_stuff = re.compile(r'\[[0-9;]+m')

if platform.node() == 'respighi':
    RESPIGHI = True
else:
    RESPIGHI = False

RES_EX_FILE = "/home/release/britney/var/data-b2/output/excuses.yaml"
if RESPIGHI:
    EX_FILE = RES_EX_FILE
    RES_FILE = "/home/elbrus/repro-regress.log"
else:
    EX_FILE = "/home/paul/tmp/excuses.yaml"
    RES_FILE = "/home/paul/tmp/repro-regress.log"

EMAIL_FROM = 'Paul Gevers <elbrus@debian.org>'
EMAIL_TO = '%(trigger)s@packages.debian.org, %(broken)s@packages.debian.org'
EMAIL_CC = EMAIL_FROM
EMAIL_BOTH_SUBJECT = '%(trigger)s breaks %(broken)s autopkgtest'
EMAIL_NEEDS_UPDATE_SUBJECT = '%(broken)s: autopkgtest needs update for new version of %(trigger)s'
EMAIL_SUBJECT_REGRESSION = '%(trigger)s: autopkgtest regression'
EMAIL_SUBJECT_NEW = '%(trigger)s: autopkgtest failure'
EMAIL_BODY_INTRO = '''
Dear maintainer(s),

With a recent upload of %(trigger)s the autopkgtest of %(broken)s fails in testing when that autopkgtest is run with the binary packages of %(trigger)s from unstable. It passes when run with only packages from testing. In tabular form:
'''
EMAIL_BODY_NEW = '''
Dear maintainer(s),

You recently added an autopkgtest to your package %(trigger)s, great. However, it fails. Currently this failure is blocking the migration to testing [1]. Can you please investigate the situation and fix it?

I copied some of the output at the bottom of this report.

More information about this bug and the reason for filing it can be found on
https://wiki.debian.org/ContinuousIntegration/RegressionEmailInformation

Paul

[1] https://qa.debian.org/excuses.php?package=%(trigger)s

'''
EMAIL_BODY_ALWAYS = '''
Dear maintainer(s),

Your package %(trigger)s has an autopkgtest, great. However, it fails. Can you please investigate the situation and fix it? I copied some of the output at the bottom of this report.

The release team has announced [1] that failing autopkgtest are now considered RC in testing.

More information about this bug and the reason for filing it can be found on
https://wiki.debian.org/ContinuousIntegration/RegressionEmailInformation

Paul

[1] https://lists.debian.org/debian-devel-announce/2019/07/msg00002.html

'''
TABLE_VER_DEP = '''
versioned deps [0]     from testing    from unstable'''
EMAIL_TABLE_TWO = '''
                       pass            fail
%(trigger)-22s from testing    %(ver)s
%(broken)-22s from testing    %(broken_ver)s%(ver_dep)s
all others             from testing    from testing
'''
EMAIL_TABLE_ONE = '''
                       pass            fail
%(trigger)-22s from testing    %(ver)s%(ver_dep)s
all others             from testing    from testing
'''
EMAIL_FOOTNOTE_0 = '''[0] You can see what packages were added from the second line of the log file quoted below. The migration software adds source package from unstable to the list if they are needed to install packages from %(trigger)s/%(ver)s. I.e. due to versioned dependencies or breaks/conflicts.
'''
EMAIL_BODY_FOOTER = '''
More information about this bug and the reason for filing it can be found on
https://wiki.debian.org/ContinuousIntegration/RegressionEmailInformation

Paul

%(footnote)s[1] https://qa.debian.org/excuses.php?package=%(trigger)s

'''

EMAIL_BOTH = '''Source: %(trigger)s, %(broken)s
Control: found -1 %(trigger)s/%(ver)s
Control: found -1 %(broken)s/%(broken_ver)s
Severity: serious
Tags: sid trixie
User: debian-ci@lists.debian.org
Usertags: breaks needs-update
''' + EMAIL_BODY_INTRO + EMAIL_TABLE_TWO + '''
I copied some of the output at the bottom of this report.

Currently this regression is blocking the migration of %(trigger)s to testing [1]. Due to the nature of this issue, I filed this bug report against both packages. Can you please investigate the situation and reassign the bug to the right package?
''' + EMAIL_BODY_FOOTER

EMAIL_NEEDS_UPDATE = '''Source: %(broken)s
Version: %(broken_ver)s
Severity: serious
X-Debbugs-CC: %(trigger)s@packages.debian.org
Tags: sid trixie
User: debian-ci@lists.debian.org
Usertags: needs-update
Control: affects -1 src:%(trigger)s
''' + EMAIL_BODY_INTRO + EMAIL_TABLE_TWO + '''
I copied some of the output at the bottom of this report.

Currently this regression is blocking the migration of %(trigger)s to testing [1]. Of course, %(trigger)s shouldn't just break your autopkgtest (or even worse, your package), but it seems to me that the change in %(trigger)s was intended and your package needs to update to the new situation.

If this is a real problem in your package (and not only in your autopkgtest), the right binary package(s) from %(trigger)s should really add a versioned Breaks on the unfixed version of (one of your) package(s). Note: the Breaks is nice even if the issue is only in the autopkgtest as it helps the migration software to figure out the right versions to combine in the tests.
''' + EMAIL_BODY_FOOTER

EMAIL_SINGLE = '''Source: %(trigger)s
Version: %(ver)s
Severity: serious
User: debian-ci@lists.debian.org
Usertags: regression
''' + EMAIL_BODY_INTRO + EMAIL_TABLE_ONE + '''
I copied some of the output at the bottom of this report.

Currently this regression is blocking the migration to testing [1]. Can you please investigate the situation and fix it?
''' + EMAIL_BODY_FOOTER

EMAIL_NEW = '''Source: %(trigger)s
Version: %(ver)s
Severity: serious
User: debian-ci@lists.debian.org
Usertags: fails-always
''' + EMAIL_BODY_NEW

EMAIL_ALWAYS = '''Source: %(trigger)s
Version: %(ver)s
Severity: serious
User: debian-ci@lists.debian.org
Usertags: fails-always
''' + EMAIL_BODY_ALWAYS


ACT = 0
DATE = 1
COM = 2
BUG = 3
VER = 5
RET = 6
TRIG = 7
RC = 8
SID = 9
MAX = 10

def update():
    try:
        new_list = _load_info()
    except FileNotFoundError:
        new_list = {}

    for pkg, info in new_list.items():
        info[ACT] = False

    with open(EX_FILE, 'r', encoding='utf-8') as stream:
        ex = yaml.safe_load(stream)
    
    stamp_num = ex['generated-date']
    stamp = stamp_num.strftime('%Y-%m-%d')
    for source in ex['sources']:
        try:
            pkg = source['item-name']
        except KeyError:
            print('no item-name: ' + str(source))
            continue
        try:
            results = source['policy_info']['reproducible']['test-results']
        except KeyError:
            # binNMU's don't get tested and don't have an autopkgtest field
            # print('no policy_info for ' + pkg + ': ' + str(source))
            continue
        for res in results:
            if not res.startswith((
                    'new but not reproducible ',
                    'not reproducible on ',
                    'reproducible ',
                    'waiting-for-test-results ',
                    )):
                if pkg not in new_list:
                    new_list[pkg] = [True, stamp, res]
#                    print("https://qa.debian.org/excuses.php?package=%s %s" % (pkg, res))
                    print("https://tests.reproducible-builds.org/debian/rb-pkg/unstable/%s/%s.html %s" \
                          % (res.split()[-1], pkg, res))
                else:
                    cur_pkg = new_list[pkg] # simpler for next lines
                    cur_pkg[ACT] = True

#    breakpoint()
    _save_info(new_list)

def find(pkg):
    list = _load_info()

    for url, data in list.items():
        for trigger, info in data.items():
            if info[ACT] and (trigger == pkg or info[VER].split('/')[0] == pkg):
                print(info[RET].split('/')[-1] + ' on ' + url.split('/')[-1] + ': ' + trigger + ' breaks ' + info[VER].split('/')[0])

def show_bugs(pkg):
    res = subprocess.run(['bts', 'select', 'source:' + pkg], text=True, capture_output=True)
    if res.returncode != 0:
        print("Couldn't run bts select")
    elif len(res.stdout) != 0:
#        print(res.stdout)
        bugs = res.stdout.split()
        if len(bugs) > 5:
            print("More than 5 bugs found, probably better looking at the web: " + \
                  "https://bugs.debian.org/src:" + pkg)
            return
        for bug in bugs:
            res = subprocess.run(['bts', 'status', bug], text=True, capture_output=True)
            if res.returncode != 0:
                print("Couldn't run bts status for bug " + bug)
                continue
            print("https://bugs.debian.org/" + bug + res.stdout.split('subject')[1].split('\n')[0])
    else:
        print('No results from "bts select ..."')

def process_oldest(auto=False):
    auto_found = False
    list = _load_info()

    oldest_date = time.strptime("2100-01-01", '%Y-%m-%d')
    improved_lookup = collections.defaultdict(dict)
    for url, data in list.items():
        tested = url.split('/')[-3]
        arch = url.split('/')[-1]
        for trigger, info in data.items():
#            if info[TRIG].startswith('r-'):
#                continue
            if not info[ACT]:
                continue
            improved_lookup.setdefault(trigger, {}).setdefault(tested, {}).setdefault(arch, info)
            improved_lookup[trigger][tested]["sid_only"] = info[SID]
            if info[BUG] is not None:
                improved_lookup[trigger][tested]["bug"] = info[BUG]
            if info[COM] != "":
                improved_lookup[trigger][tested]["comment"] = info[COM]
            if "date" not in improved_lookup[trigger][tested].keys():
                improved_lookup[trigger][tested]["date"] = info[DATE]

    for trigger, data1 in improved_lookup.items():
        for tested, data2 in data1.items():
            if not data2["sid_only"] and \
               "bug" not in data2 and \
               "comment" not in data2 and \
               time.strptime(data2["date"], '%Y-%m-%d') < oldest_date:
                oldest_trigger = trigger
                oldest_tested = tested
                oldest_data = data2
                oldest_date = time.strptime(data2["date"], '%Y-%m-%d')

    # pull bugs
    # pull log
    # split log
    # get pinned packages
    # check for fallback
    # get first failing test
    prefix = srchash(oldest_tested)
    stop = False
    pin_packages = []

    if oldest_trigger != oldest_tested and not auto:
        res = subprocess.run(['rmadison', '-s', 'testing,unstable', oldest_tested], text=True, capture_output=True)
        if res.returncode != 0:
            print("Couldn't run rmadison")
            exit(1)
        if len(res.stdout) != 0:
            print(res.stdout, end='')

    auto_round = 1
    while not stop:
        print()
        if oldest_trigger != oldest_tested:
            print("trigger: " + oldest_trigger + "  https://tracker.debian.org/" + oldest_trigger)
            print("test: ", end='')
        print(oldest_tested + "  https://tracker.debian.org/" + oldest_tested)

        for arch in oldest_data:
            # I'm only interested in the real archs
            if arch not in ["bug", "comment", "date", "sid_only"]:
                print(arch + ' ', end='')
        print()
        print(time.strftime("%Y-%m-%d", oldest_date))

        if len(pin_packages) > 1:
            print("Other packages pinned")
        elif len(pin_packages) == 1:
            print("No other packages pinned")

        if auto:
            if auto_round == 1:
                answer = 'a'
            elif auto_round == 2:
                answer = 't'
            else:
                return False
            auto_round += 1
        else:
            answer = input('Comment/Show bugs trigger/show Bugs test/Get log/Report bug/report "N" bug/Update bug number (Auto trigger/needs-updaTe)/Quit:\n').lower()
        if answer in ["a", "t"]:
            if answer == "a":
                res = subprocess.run(['bts', 'select', 'usertag:breaks','usertag:regression', 'usertag:fails-always', 'user:debian-ci@lists.debian.org', 'source:' + oldest_trigger], text=True, capture_output=True)
            else:
                res = subprocess.run(['bts', 'select', 'usertag:needs-update', 'user:debian-ci@lists.debian.org', 'source:' + oldest_tested], text=True, capture_output=True)
            if res.returncode != 0:
                print("Couldn't run bts select")
                exit(1)
            if len(res.stdout) != 0:
                bugs = res.stdout.strip().split('\n')
                if len(bugs) == 1:
                    number = bugs[0]
                    print('Updating bug number: ' + number)
                else:
                    print('Multiple bugs found for usertag, skipping autodetection.')
                    continue
            else:
                print("No bugs found for usertag.")
                continue
        elif answer == "c":
            comment = input('What comment should be added to the log file? (TODO and CHECK are special)\n')
            for arch in oldest_data:
                try:
                    list['https://ci.debian.net/packages/' + prefix + '/' + \
                         oldest_tested +  '/testing/' + arch][oldest_trigger][COM] = \
                        comment
                except KeyError:
                    pass
            stop = True
        elif answer in ["g", "r", "n"]:
            try:
                x = log_nr
            except UnboundLocalError:
                for arch in oldest_data:
                    # I'm only interested in the first log, so the for loop is a bit stupid
                    log_nr = oldest_data[arch][RET].split('/')[-1]
                    break
                [pin_packages, first_fail_log, url_used] = process_log(oldest_tested, arch, log_nr)
                if len(first_fail_log) > 5000:
                    print(first_fail_log[-5000:])
                else:
                    print(first_fail_log)
            if answer == "r":
                reportbug(log_nr,
                          extra_text=url_used + '\n\n' + first_fail_log,
                          other_pkgs=len(pin_packages)>1)
            elif answer == "n":
                reportbug(log_nr,
                          needs_update=True,
                          extra_text=url_used + '\n\n' + first_fail_log,
                          other_pkgs=len(pin_packages)>1)
        elif answer == "q":
            stop = True
        elif answer == "s":
            show_bugs(oldest_trigger)
        elif answer == "b":
            show_bugs(oldest_tested)
        elif answer == "u":
            number = input('Which bug number should be added to the log file?\n')

        if answer in ["a", "t", "u"]:
            for arch in oldest_data:
                try:
                    list['https://ci.debian.net/packages/' + prefix + '/' + \
                          oldest_tested +  '/testing/' + arch][oldest_trigger][BUG] = \
                        int(number)
                    auto_found = True
                except KeyError:
                    pass
            stop = True


    _save_info(list)
    if auto:
        return auto_found
    exit()


def fixup():
    raise(KeyError('Can only run once'))
    try:
        new_list = _load_info()
    except FileNotFoundError:
        new_list = {}

    for url, data in new_list.items():
        for pkg, info in data.items():
            info.append(0)
            info.append(False)

    _save_info(new_list)

def show_unannotated(todo=False, quiet=False, check=False):
    regr_info = _load_info()

    combos = set()
    for url, data in regr_info.items():
        seen = False
        test = url.split("/")[5]
        for pkg, info in data.items():
            show = False
            if todo:
                show = show or (info[ACT] and info[COM].startswith('TODO'))
            if check:
                show = show or (info[ACT] and info[COM].startswith('CHECK'))
            if not quiet:
                show = show or (info[ACT] and info[BUG] is None and info[COM] == '')
            if show:
                if (test, pkg) in combos:
                    continue
                combos.add((test, pkg))
                if not seen:
                    print(url)
                    seen = True
                print('    ' + info[TRIG])
                if info[COM] != '':
                    print('      ' + info[COM])
                if info[BUG] is not None:
                    print('      https://bugs.debian.org/' + str(info[BUG]))
                print('      https://tracker.debian.org/pkg/' + pkg)
                print('      ' + info[RET])
        if seen:    
            print()

def retrigger(dry_run=False):
    regr_info = _load_info()
    for url, data in regr_info.items():
        test_pkg = url.split('/')[-3]
        ref_triggered = False
        for pkg, info in data.items():
            trigger = info[TRIG]
            if info[ACT] and info[BUG] is None and (info[COM] == '' or info[COM].startswith('AUTOTRIGGERED ')):
                run_id = info[RET].split('/')[-1]
                if not ref_triggered and not info[COM].startswith('AUTOTRIGGERED '):
                    print('Retriggering reference for %s' %(test_pkg))
                    if not dry_run:
                        subprocess.run(['bin/ci-generate-reference', test_pkg])
                    ref_triggered = True
                print('Retriggering %s, %s for %s' %(run_id, test_pkg, trigger))
                if not dry_run:
                    subprocess.run(['bin/ci-retrigger', run_id])
                    info[COM] = 'AUTOTRIGGERED ' + time.strftime('%m%d') + ' ' + info[COM]
                    _save_info(regr_info)
    
def clean():
    regr_info = _load_info()
    new_info = copy.deepcopy(regr_info) # copy because we are going to change the content
    for url, data in regr_info.items():
        for pkg, info in data.items():
            if not info[ACT]:
                new_info[url].pop(pkg)
            if new_info[url] == {}:
                new_info.pop(url)

    _save_info(new_info)

def srchash(src):
    '''archive hash prefix for source package'''

    if src.startswith('lib'):
        return src[:4]
    else:
        return src[0]

def fails_always_bug(package):
    query = "rmadison --suite=testing %s" % package
    res = subprocess.run(query, text=True, capture_output=True, check=True, shell=True)
    if res.returncode != 0:
        print("rmadison didn't return OK")
        exit(1)
    print(res.stdout)
    ver = res.stdout.split()[2]
    msg = EmailMessage()
    msg['From'] = EMAIL_FROM
    msg['To'] = 'submit@bugs.debian.org'
    msg['Subject'] = EMAIL_SUBJECT_NEW % {'trigger': package}
    msg.set_content(EMAIL_ALWAYS % {'trigger': package, 'ver': ver})
    with open('/tmp/message.eml', 'w') as f:
        print(msg, file=f)
    subprocess.run(['thunderbird', '/tmp/message.eml'])

def reportbug(run_id, needs_update=False, extra_text="", other_pkgs=False):
    regr_info = _load_info()

    if extra_text == '':
        extra_link = 'https://ci.debian.net/data/autopkgtest/testing/' + \
            '%(arch)s/%(b)s/%(broken)s/%(run_id)s/log.gz'
    else:
        extra_link = ''

    for url, data in regr_info.items():
        for pkg, info in data.items():
            if run_id != info[RET].split('/')[-1]:
                continue
            arch = url.split('/')[-1]
            (trigger, ver) = info[TRIG].split('/')
            (broken, broken_ver) = info[VER].split('/')
            if other_pkgs:
                ver_dep = TABLE_VER_DEP
                footnote = EMAIL_FOOTNOTE_0 % {'trigger': trigger, 'ver': ver}
            else:
                ver_dep = ''
                footnote = ''
            msg = EmailMessage()
            msg['From'] = EMAIL_FROM
            msg['To'] = 'submit@bugs.debian.org'
            if trigger == broken and needs_update:
                msg['Subject'] = EMAIL_SUBJECT_NEW % {
                    'trigger': trigger, 'ver': ver,
                    'broken': broken, 'broken_ver': broken_ver}
                msg.set_content((EMAIL_NEW + extra_link) % {
                    'trigger': trigger, 'ver': ver, 'broken': broken,
                    'b': srchash(broken), 'broken_ver': broken_ver,
                    'run_id': run_id, 'arch': arch} + extra_text)
            elif trigger == broken:
                msg['Subject'] = EMAIL_SUBJECT_REGRESSION % {
                    'trigger': trigger, 'ver': ver,
                    'broken': broken, 'broken_ver': broken_ver}
                msg.set_content((EMAIL_SINGLE + extra_link) % {
                    'trigger': trigger, 'ver': ver, 'broken': broken,
                    'b': srchash(broken), 'broken_ver': broken_ver,
                    'run_id': run_id, 'arch': arch, 'ver_dep': ver_dep,
                    'footnote': footnote} + extra_text)
            elif needs_update:
                msg['Subject'] = EMAIL_NEEDS_UPDATE_SUBJECT % {
                    'trigger': trigger, 'ver': ver,
                    'broken': broken, 'broken_ver': broken_ver}
                msg.set_content((EMAIL_NEEDS_UPDATE + extra_link) % {
                    'trigger': trigger, 'ver': ver, 'broken': broken,
                    'b': srchash(broken), 'broken_ver': broken_ver,
                    'run_id': run_id, 'arch': arch, 'ver_dep': ver_dep,
                    'footnote': footnote} + extra_text)
            else:
                msg['Subject'] = EMAIL_BOTH_SUBJECT % {
                    'trigger': trigger, 'ver': ver,
                    'broken': broken, 'broken_ver': broken_ver}
                msg.set_content((EMAIL_BOTH + extra_link) % {
                    'trigger': trigger, 'ver': ver, 'broken': broken,
                    'b': srchash(broken), 'broken_ver': broken_ver,
                    'run_id': run_id, 'arch': arch, 'ver_dep': ver_dep,
                    'footnote': footnote} + extra_text)
            with open('/tmp/message.eml', 'w') as f:
                print(msg, file=f)
            break
    subprocess.run(['thunderbird', '/tmp/message.eml'])

def generate_html(local_only=False):
    ref_time = time.time() - 86400 * 45
    regr_info = _load_info()
    content_broken = ''
    content_trigger = ''
    stamp = time.strftime('%Y-%m-%d %H:%M %Z')
    page_template = Template(
'''<!DOCTYPE html>
<html><head>
<title>elbrus' log of regressions</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<h2>Regressions sorted by broken source package name</h2>
<table>
<tr><th align="left">Package</th><th align="left">Trigger</th><th align="left">#RC</th><th align="left">Bug #</th><th align="left">First seen</th><th align="left">retry</th></tr>
$content_broken
</table>
<h2>Regressions sorted by trigger</h2>
<table>
<tr><th align="left">Package</th><th align="left">Trigger</th><th align="left">#RC</th><th align="left">Bug #</th><th align="left">First seen</th><th align="left">retry</th></tr>
$content_trigger
</table>
<p>This page was generated from a log file kept by elbrus on $stamp.</p>
</body></html>
''')
    bug_template = Template(
'''<tr bgcolor=$color>
<td><a href="$url">$pkg</a> ($arch)</td>
<td><a href="https://tracker.debian.org/pkg/$tr_src">$trigger</a></td>
<td align="center"><a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?archive=no&pend-exc=pending-fixed&pend-exc=fixed&pend-exc=done&sev-inc=critical&sev-inc=grave&sev-inc=serious&src=$pkg">$rc</a></td>
<td><a href="https://bugs.debian.org/$bug">$bug</a></td>
<td>$seen</td>
<td><a href="$retry">♻</a></td>
</tr>
''')
    no_bug_template = Template(
'''<tr bgcolor=$color>
<td><a href="$url">$pkg</a> ($arch)</td>
<td><a href="https://tracker.debian.org/pkg/$tr_src">$trigger</a></td>
<td align="center"><a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?archive=no&pend-exc=pending-fixed&pend-exc=fixed&pend-exc=done&sev-inc=critical&sev-inc=grave&sev-inc=serious&src=$pkg">$rc</a></td>
<td>---</td>
<td>$seen</td>
<td><a href="$retry">♻</a></td>
</tr>
''')

    broken_seen = collections.defaultdict(dict)
    for url, data in sorted(regr_info.items()):
        pkg = url.split('/')[-3]
        arch = url.split('/')[-1]
        for tr_src, info in data.items():
            pkg_tr_src = (pkg, tr_src)
            if local_only and info[TRIG].startswith('r-'):
                continue
            if local_only and info[TRIG].startswith('haskell-'):
                continue
            if info[ACT]:
                if info[SID]:
                    color="lightgrey"
                    if info[RC] > 0:
                        continue
                    if local_only:
                        continue
                else:
                    color="white"
                try:
                    broken_seen[pkg_tr_src]['archs'] += ", " + arch
                    broken_seen[pkg_tr_src]['seen'].append(info[DATE])
                except KeyError:
                    this_one = broken_seen.setdefault(pkg_tr_src, {})
                    this_one.setdefault('url', url)
                    this_one.setdefault('pkg', pkg)
                    this_one.setdefault('archs', arch)
                    this_one.setdefault('tr_src', tr_src)
                    this_one.setdefault('trigger', info[TRIG])
                    this_one.setdefault('seen', [])
                    this_one['seen'].append(info[DATE])
                    this_one.setdefault('rc', info[RC])
                    this_one.setdefault('color', color)
                    this_one.setdefault('retry', info[RET])
                if info[BUG] is not None:
                    broken_seen[pkg_tr_src].setdefault('bug', info[BUG])

    for _, broken in sorted(broken_seen.items()):
        if broken['archs'] == "amd64, arm64, armel, armhf, i386, ppc64el, s390x":
            broken['archs'] = "everywhere"
        broken['seen'].sort()
        if local_only and calendar.timegm(time.strptime(broken['seen'][-1], '%Y-%m-%d')) < ref_time:
            continue
        try:
            content_broken += bug_template.safe_substitute(
                url=broken['url'],
                pkg=broken['pkg'],
                arch=broken['archs'],
                tr_src=broken['tr_src'],
                trigger=broken['trigger'],
                bug=broken['bug'],
                seen=broken['seen'][0],
                rc=broken['rc'],
                color=broken['color'],
                retry=broken['retry'])
        except KeyError:
            content_broken += no_bug_template.safe_substitute(
                url=broken['url'],
                pkg=broken['pkg'],
                arch=broken['archs'],
                tr_src=broken['tr_src'],
                trigger=broken['trigger'],
                seen=broken['seen'][0],
                rc=broken['rc'],
                color=broken['color'],
                retry=broken['retry'])

    # Quick and dirty way to be able to sort on triggers
    trigger_seen = collections.defaultdict(dict)
    for broken in broken_seen.values():
        trigger_seen[(broken['tr_src'], broken['pkg'])] = broken

    for _, broken in sorted(trigger_seen.items()):
        if local_only and calendar.timegm(time.strptime(broken['seen'][-1], '%Y-%m-%d')) < ref_time:
            continue
        try:
            content_trigger += bug_template.safe_substitute(
                url=broken['url'],
                pkg=broken['pkg'],
                arch=broken['archs'],
                tr_src=broken['tr_src'],
                trigger=broken['trigger'],
                bug=broken['bug'],
                seen=broken['seen'][0],
                rc=broken['rc'],
                color=broken['color'],
                retry=broken['retry'])
        except KeyError:
            content_trigger += no_bug_template.safe_substitute(
                url=broken['url'],
                pkg=broken['pkg'],
                arch=broken['archs'],
                tr_src=broken['tr_src'],
                trigger=broken['trigger'],
                seen=broken['seen'][0],
                rc=broken['rc'],
                color=broken['color'],
                retry=broken['retry'])

    content = page_template.safe_substitute(
        content_broken=content_broken,
        content_trigger=content_trigger,
        stamp=stamp)

    tmp_file = '/tmp/regressions.html'
    with open(tmp_file, 'w') as f:
        f.write(content)
    if not local_only:
        res = subprocess.run(['scp', tmp_file, 'people.debian.org:public_html/ci/'])
        if res.returncode != 0:
            print('Copying didn''t work')
            exit(1)
        
def _load_info():
    with open(RES_FILE) as f:
        regr_info = json.load(f)
    return regr_info

def _save_info(new_list):
    with open(RES_FILE, 'w') as f:
        json.dump(new_list, f, indent=4)

def process_log(tested, arch, log_nr):
#    log_nr = '16084634'
#    tested = 'rust-derive-builder-core'
#    arch = 'arm64'
    prefix = srchash(tested)
    url = 'https://ci.debian.net/data/autopkgtest/testing/' + arch + '/' + \
        prefix + '/' + tested + '/' + log_nr + '/log.gz'

    import urllib.request
    import gzip
    response = urllib.request.urlopen(url)
    content = gzip.decompress(response.read()).decode(errors='replace')
    pieces = content.split(" @@@@@@@@@@@@@@@@@@@@ ")
    # find the pin-packages (which end with a space), packages are separated by `,`.
    pin_packages = pieces[0].split('--pin-packages=unstable=')[1].split()[0].split(',')
    summary = pieces[-1].strip('summary\n').split('\n')

    pieces = re.split(r": -----------------------]|badpkg: Test dependencies are unsatisfiable. A common reason is that your testbed is out of date with respect to the archive, and you need to use a current testbed or run apt-get update or use -U.", content)

    ii = -1
    has_uninstallable_tests = False
    first_fail_log = ""
    for line in summary:
        if line.startswith(("blame: ", "badpkg: ")):
            has_uninstallable_tests = True
            ii += 1
            continue
        if "SKIP" not in line:
            ii += 1
        if "FAIL" in line:
            try:
                first_fail_log = pieces[ii].split('[-----------------------\n')[1]
                break
            except IndexError:
                pass

    if has_uninstallable_tests and first_fail_log == "":
        first_fail_log = pieces[0]
    first_fail_log = shell_stuff.sub('', first_fail_log)
    if " FAILURES " in first_fail_log:
        first_fail_log = re.sub(r'.*=================================== FAILURES ', '=================================== FAILURES ', first_fail_log, flags=re.S)
    return (pin_packages, first_fail_log, url)


if __name__ == '__main__':
    parser = optparse.OptionParser(version="%prog")
    parser.add_option("-a", "--auto", action="store_true", dest="autoupdate", default=False,
                          help="Run auto bug number finding for items until first unresolved one")
    parser.add_option("-b", "--bug", dest="bug",
                          help="report a bug against package(s)")
    parser.add_option(      "--check", action="store_true", dest="check", default=False,
                          help="show items that were annotated with CHECK")
    parser.add_option(      "--clean", action="store_true", dest="clean", default=False,
                          help="remove items that aren't in the latest excuses file")
    parser.add_option("-d", "--dry-run", action="store_true", dest="dry_run", default=False,
                          help="only show what would be happening")
    parser.add_option("-f", "--find", dest="find",
                          help="find items involving a certain package")
    parser.add_option("-g", "--generate-html", action="store_true", dest="html", default=False,
                          help="generate html on people.debian.org")
    parser.add_option("-l", "--local-only", action="store_true", dest="local_only", default=False,
                          help="with generate-html: reduced html whihc isn't uploaded")
    parser.add_option("-n", "--needs-update", action="store_true", dest="needs_update", default=False,
                          help="only file the bug against the package that breaks")
    parser.add_option("-o", "--oldest", action="store_true", dest="old", default=False,
                          help="show oldest unprocessed items and ask questions")
    parser.add_option("-p", "--package", dest="package",
                          help="file a fails-always bug against the package")
    parser.add_option("-q", "--quiet", action="store_true", dest="quiet", default=False,
                          help="don't show the output")
    parser.add_option("-r", "--retrigger", action="store_true", dest="retrigger", default=False,
                          help="retrigger regressions and (if not done so yet) the reference")
    parser.add_option("-t", "--todo", action="store_true", dest="todo", default=False,
                          help="show items that were annotated with TODO")
    parser.add_option(      "--fixup", action="store_true", dest="fixup", default=False,
                          help="Fixup")
    parser.add_option("-u", "--update", action="store_true", dest="update", default=False,
                          help="update the regression log with a fresh excuses.yaml")
    (options, args) = parser.parse_args()
    if options.autoupdate:
        while process_oldest(auto=True):
            pass
    if options.old:
        process_oldest()
    if options.fixup:
        fixup()
        exit()
    if options.update:
        update()
        exit()
    if options.find:
        find(options.find)
        exit()
    if options.html:
        generate_html(options.local_only)
    if options.clean:
        clean()
    if options.retrigger:
        retrigger(options.dry_run)
    if options.package:
        fails_always_bug(options.package)
    if options.bug:
        options.quiet = True
        reportbug(options.bug, options.needs_update)
    show_unannotated(todo=options.todo,
                     quiet=options.quiet, check=options.check)
